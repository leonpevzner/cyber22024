from flask import Flask, render_template, request, redirect
import sqlalchemy
import pandas as pd
import random
import os
from hashlib import sha256
import subprocess
from shlex import quote


engine = sqlalchemy.create_engine('sqlite:///app.db')
app = Flask(__name__)


@app.route('/', methods=['GET'])
def main():
    return render_template('index.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/login/<error>')
def login_error(error):
    return render_template('login.html', error=error)

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/register/<error>')
def register_error(error):
    return render_template('register.html', error=error)

anti_bot = {}
anti_bot_limit = 5

@app.route('/dashboard-no-salt-secure', methods=['POST'])
def dashboard_no_salt_secure():
    # Anti bot - get remote address
    remote_address = request.remote_addr
    if remote_address in anti_bot:
        anti_bot[remote_address] += 1
    else:
        anti_bot[remote_address] = 1

    if anti_bot[remote_address] > anti_bot_limit:
        return redirect('/login/Too many requests')
    username = request.form.get('username')
    password = request.form.get('password')
    user = pd.read_sql(
        "SELECT * FROM users WHERE username=? AND password=?",
        params=(username, password), con=engine)
    if user.empty:
        return redirect('/login/Invalid username or password')
    image_path = user['first_name'][0] + '_' + user['last_name'][0] + '.jpeg'
    # windows 
    # res = os.popen(f'dir static\{image_path}')
    # res_text = res.read()
    # print(f'{res_text=}')
    # if '1 File(s)' not in res_text:
    #     image_path = 'default.png'
    # res.close()
    # unix
    # res = subprocess.Popen(['ls', f'static/{image_path}'], shell=False)
    # res_text = res.stdout
    res = os.popen(f'ls static/{quote(image_path)}')
    # res = os.popen(f'ls static/{image_path}')
    res_text = res.read()   
    if not res_text:
        image_path = 'default.jpeg'
    return render_template('dashboard.html', first_name=user['first_name'][0], image_path=image_path)

@app.route('/dashboard-no-salt', methods=['POST'])
def dashboard_no_salt():
    username = request.form.get('username')
    password = request.form.get('password')
    user = pd.read_sql(
        f"SELECT * FROM users WHERE username='{username}' AND password='{password}'", engine)
    if user.empty:
        return redirect('/login/Invalid username or password')
    return render_template('dashboard.html' , first_name=user['first_name'][0])

@app.route('/dashboard', methods=['POST'])
def dashboard():
    username = request.form.get('username')
    password = request.form.get('password')

    user = pd.read_sql(f"SELECT * FROM users WHERE username='{username}'", engine)
    if user.empty:
        return redirect('/login/Invalid username')
    username = user['username'][0]
    salt = user['salt'][0]
    password = f"{password}{salt}"
    hashed_password = sha256(password.encode()).hexdigest()
    if hashed_password != user['password'][0]:
        return redirect('/login/Invalid password')
    return render_template('dashboard.html' , first_name=user['first_name'][0])

@app.route('/add-user-no-salt', methods=['POST'])
def add_user_no_salt():
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    username = request.form.get('username')
    password = request.form.get('password')
    if len(password) < 4:
        return redirect('/register/Password must be longer than 4 characters')
    user = pd.read_sql(f"SELECT * FROM users WHERE username='{username}'", engine)
    if not user.empty:
        return redirect('/register/Username already exists')
    df = pd.DataFrame({
        'first_name': [first_name], 
        'last_name': [last_name],
        'username': [username], 
        'password': [password]})
    df.to_sql('users', engine, if_exists='append', index=False)
    return redirect('/')

@app.route('/add-user', methods=['POST'])
def add_user():
    first_name = request.form.get('first_name')
    username = request.form.get('username')
    password = request.form.get('password')
    if len(password) < 4:
        return redirect('/register/Password must be longer than 4 characters')
    user = pd.read_sql(f"SELECT * FROM users WHERE username='{username}'", engine)
    if not user.empty:
        return redirect('/register/Username already exists')
    salt = random.randint(1000, 9999)
    password = f"{password}{salt}"
    hashed_password = sha256(password.encode()).hexdigest()
    df = pd.DataFrame({
        'first_name': [first_name], 
        'username': [username], 
        'salt': [salt],
        'password': [hashed_password]})
    df.to_sql('users', engine, if_exists='append', index=False)
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True)