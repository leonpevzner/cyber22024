from flask import Flask, render_template

app = Flask(__name__)

@app.route('/nidp/saml2/sso', methods=['GET'])
def home():
    return render_template('net_src.html')

if __name__ == '__main__':
    app.run(debug=True)
